cp -rf content themes/hugo-travelify-theme/exampleSite/
cp -rf static themes/hugo-travelify-theme/exampleSite/
cp -rf data themes/hugo-travelify-theme/exampleSite/
cp -rf config.toml themes/hugo-travelify-theme/exampleSite/
hugo -d balaramadurai.github.io
cd balaramadurai.github.io
git add .
git commit -m "Changes to reflect changes in the theme"
cd ../themes/hugo-travelify-theme
git add .
git commit
